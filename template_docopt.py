#!/usr/bin/env python3


"""Process FILE or use SANDBOX

Usage:
  {prog} [-v] FILE
  {prog} -s SANDBOX
  {prog} (-h | --help | --version)

Arguments:
  FILE         input file
  -s SANDBOX   Use SANDBOX

Options:
  -v --verbose   Verbose mode to show detailed info
  -h --help      Show this help.
  --version      Show version.

Examples:
  {prog} sample.txt
  {prog} -s my_sandbox


More Info:
  see http://docopt.org/
  see https://github.com/docopt
    Python: https://github.com/jazzband/docopt-ng
            old: https://github.com/docopt/docopt
    bash : https://github.com/docopt/docopts
    C++ : https://github.com/docopt/docopt.cpp
"""


# standard
import sys

# external
from docopt import docopt

# local


# module globals and constants
__version__ = "0.6.2"
__author__ = "First_Name LAST_NAME"
# replace prog by program name
__doc__ = __doc__.format(prog=sys.argv[0])


class MyClass:
    """class example
    if a class contains only one method except init
    it means you needn't a class ;)

    si une classe ne contient qu'une méthode en plus de init
    c'est que vous n'avez pas besoin de classe ;)
    """

    def __init__(self, param1, param2="default value"):
        self.x = param1
        self.y = param2


def main(args):
    # by default __debug__=True
    # if python3 -O then __debug__=False
    # print(f"{__debug__=}")

    if __debug__ or args["--verbose"]:
        print(args)

    # if FILE argument is present and contains 'm'
    # si l'argument FILE est présent et contient 'm'
    if args["FILE"]:
        if "m" in args["FILE"]:
            print("{} contient m".format(args["FILE"]))
    else:
        print(args["-s"])


if __name__ == "__main__":
    arguments = docopt(__doc__, version=__version__)
    main(arguments)
