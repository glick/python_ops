#!/usr/bin/env python3

"""
{prog} compte le nombre de CODE dans le fichier FILE au format d'un log nginx

Usage:
  {prog} [options] CODE FILE
  {prog} -e PATTERN FILE
  {prog} (-h | --help | --version)

Arguments:
  CODE          http code status à rechercher
  FILE          input file
  -e PATTERN    recherche les codes correspondant
                à l'expression régulière PATTERN

Options:
  -g, --group-by-destination    compte le nb de code
                                en les regroupant par destination
  -h --help    Show this help.
  --version    Show version.

Exemples:
  {prog} 404 nginx.log
  {prog} -g 404 nginx.log
  {prog} -e 40[0-9] nginx.log
"""


# standard library imports
import re
from pprint import pprint
import sys

# related third party imports
from docopt import docopt


# local application/library specific imports


# module globals and constants
__version__ = "0.1"
__author__ = "Gael Lickindorf"
# replace prog by program name
__doc__ = __doc__.format(prog=sys.argv[0])
# indice de la colonne où se trouve le statut (la numérotation commence à 0)
STATUS_INDEX = 8


def nb_code(code, fichier):
    """Renvoie le nombre de code présent dans le fichier"""
    nombre_code = 0
    with open(fichier) as log_file:
        for line in log_file:
            if code in line:
                nombre_code += 1
    return nombre_code


def nb_champ_code(code, file):
    """Renvoie le nombre de code présent dans le champ code du fichier file"""
    nombre_code = 0
    with open(file) as log_file:
        for line in log_file:
            line = line.strip()  # supprime la fin de ligne \n
            # conversion de chaine en liste basée sur séparateur " "
            line = line.split()
            # print(line[STATUS_INDEX]) # trace de debug pour vérifier l'index
            if line[STATUS_INDEX] == code:
                nombre_code += 1
    return nombre_code


def nb_champ_code_groupby(code, file):
    """Renvoie pour chaque url de destination le nombre de code présent dans le champ code du fichier file"""
    # indice de la colonne où se trouve la destination
    DESTINATION_INDEX = 6
    nombre_code = {}
    with open(file) as log_file:
        for line in log_file:
            line = line.strip()  # supprime la fin de ligne \n
            # conversion de chaine en liste basée sur séparateur " "
            line = line.split()
            # print(line[STATUS_INDEX]) # trace de debug pour vérifier l'index
            if line[STATUS_INDEX] == code:
                nombre_code[line[DESTINATION_INDEX]] = (
                    nombre_code.get(line[DESTINATION_INDEX], 0) + 1
                )
    return nombre_code


def nb_pattern(pattern, file):
    """Renvoie le nombre de code correspondant au pattern dans le champ code du fichier file"""
    nombre_code = 0
    expression = re.compile(pattern)
    with open(file) as log_file:
        for line in log_file:
            line = line.strip()  # supprime la fin de ligne \n
            # conversion de chaine en liste basée sur séparateur " "
            line = line.split()
            # print(line[STATUS_INDEX]) # trace de debug pour vérifier l'index
            if expression.search(line[STATUS_INDEX]):
                nombre_code += 1
    return nombre_code


def main(code, file, pattern="", group=False):
    """Affiche le nombre de code dans le fichier file
    ou le nombre de code correspondant au pattern
    et les groupe par destination si group est vrai"""
    # print(f"code={code} file={file} pattern={pattern} group={group}")
    if code:
        if group:
            pprint(nb_champ_code_groupby(code, file))
        else:
            print(nb_champ_code(code, file))
    elif pattern:
        print(nb_pattern(pattern, file))


if __name__ == "__main__":
    ARGS = docopt(__doc__, version=__version__)
    # print(ARGS)
    main(ARGS["CODE"], ARGS["FILE"], ARGS["-e"], ARGS["--group-by-destination"])
    # exit(1)
