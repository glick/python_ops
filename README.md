# python_ops

formation python pour les ops


## Comment générer la présentation ?
```bash
git clone https://github.com/hakimel/reveal.js.git
cd reveal.js
# list tags and select one for instance 3.8.0
git tag
git checkout 3.8.0
cd ..
jupyter nbconvert python_day_1.ipynb --to slides --reveal-prefix reveal.js

curl https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.8.0/lib/js/head.min.js -o reveal.js/lib/js/head.min.js
curl https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.8.0/lib/js/classList.js -o reveal.js/lib/js/classList.js
```

## Comment lancer la présentation ?
Ouvrir directement le fichier python_day_1.slides.html dans un navigateur  
ou Créer un serveur web local et ouvrir http://127.0.0.1:8000/python_day_1.slides.html :
```bash
jupyter nbconvert python_day_1.ipynb --to slides --reveal-prefix reveal.js --post serve
```

## Comment lancer JupyterLab ?
```bash
jupyter lab --notebook-dir=<PROJECT_DIR>

# sans token:
cd <PROJECT_DIR>
sudo docker run --rm -it -v `pwd`:/home/jovyan/work -p 8888:8888 jupyter/base-notebook:latest start.sh jupyter lab --LabApp.token=''
```

## Comment corriger `docker: Error response from daemon ...` ?
```bash
$ sudo docker run --rm -it -v /home/admin/python_ops:/home/jovyan/work -p 8888:8888 jupyter/scipy-notebook:latest  start.sh jupyter lab --LabApp.token=''
docker: Error response from daemon: driver failed programming external connectivity on endpoint charming_visvesvaraya (e502b77dfc98d8c2c863ae6633221be96984385addb0f3c7c871411fa3a23b35):  (iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 8888 -j DNAT --to-destination 172.17.0.2:8888 ! -i docker0: iptables: No chain/target/match by that name.
 (exit status 1)).

# Correction sous CentOS 7
$ sudo systemctl start firewalld
```
