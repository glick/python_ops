import os

import pytest

from template_docopt import main


# @pytest.mark.skip(reason="Test not implemented")
@pytest.mark.parametrize(
    "args, expected",
    [
        (
            {"-s": "my_sandbox", "FILE": None},
            "{'-s': 'my_sandbox', 'FILE': None}\nmy_sandbox",
        ),
    ],
)
def test_main(args, expected, capsys):
    main(args)
    capture = capsys.readouterr()
    resultat_affiche = capture.out.strip()  # supprime `\n`
    assert resultat_affiche == str(expected)
    assert capture.err == ""


@pytest.mark.skip(reason="Test not implemented")
@pytest.mark.parametrize(
    "arguments, expected",
    [
        ("arg1", "1\n"),
    ],
)
def test_cli(arguments, expected, capfd):
    os.system(f"./template_docopt.py {arguments}")
    out, err = capfd.readouterr()
    assert out == expected
    assert err == ""
