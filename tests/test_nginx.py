# pylint: disable=missing-function-docstring

# standard
import os


# extern
import pytest


# local
from nginx import nb_code, main, nb_champ_code, nb_pattern, nb_champ_code_groupby


def test_nb_code():
    code = "404"
    fichier = "tests/data/nginx_10.log"
    assert nb_code(code, fichier) == 2


@pytest.mark.parametrize(
    "code, file, expected",
    [
        ("404", "tests/data/nginx_404_not_in_status.log", 1),
    ],
)
def test_nb_champ_code(code, file, expected):
    assert nb_champ_code(code, file) == expected


@pytest.mark.parametrize(
    "code, file, expected",
    [
        (
            "404",
            "tests/data/nginx_404_1_product_1_2_product_2.log",
            {"/downloads/product_1": 1, "/downloads/product_2": 2},
        ),
    ],
)
def test_nb_champ_code_groupby(code, file, expected):
    assert nb_champ_code_groupby(code, file) == expected


@pytest.mark.parametrize(
    "pattern, file, expected",
    [
        ("40[0-9]", "tests/data/nginx_400_410.log", 10),
    ],
)
def test_nb_pattern(pattern, file, expected):
    assert nb_pattern(pattern, file) == expected


@pytest.mark.parametrize(
    "code, file, pattern, group, expected",
    [
        ("404", "tests/data/nginx_10.log", "", False, 2),
    ],
)
def test_main(code, file, pattern, group, expected, capsys):
    main(code, file, pattern, group)
    capture = capsys.readouterr()
    resultat_affiche = capture.out.strip()  # supprime `\n`
    assert resultat_affiche == str(expected)
    assert capture.err == ""


@pytest.mark.parametrize(
    "arguments, expected",
    [
        ("404 tests/data/nginx_404_not_in_status.log", "1\n"),
        ("-e 40[0-9] tests/data/nginx_400_410.log", "10\n"),
        (
            "-g 404 tests/data/nginx_404_1_product_1_2_product_2.log",
            """{'/downloads/product_1': 1, '/downloads/product_2': 2}\n""",
        ),
    ],
)
def test_cli(arguments, expected, capfd):
    os.system(f"./nginx.py {arguments}")
    out, err = capfd.readouterr()
    assert out == expected
    assert err == ""
